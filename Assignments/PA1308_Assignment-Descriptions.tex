\documentclass[times,10pt,onecolumn]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{cite}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage[utf8x]{inputenc}
%\usepackage[swedish]{babel}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}

\title{PA1308 Assignment descriptions}
\author{Michael Unterkalmsteiner, Nauman bin Ali, Mikael Svahnberg\\
Blekinge Institute of Technology\\
SE-371 79 Karlskrona SWEDEN\\
\{mun,nal,msv\}@bth.se}
%\date{}                  % Activate to display a given date or no date

\begin{document}
\maketitle
\begin{abstract}
This document describes the five assignments in the course PA1308 Software
Architecture and Quality, autumn 2012 edition, offered at Blekinge Institute of 
Technology. 
\end{abstract}

\section{Introduction}
At the start of the course, you are expected to form groups of four.
As a group, you work on assignments 1, 2, 3 and 4. On assignment 5 you work
individually. You will receive feedback (written for groups, and
addressing general challenges, during the lectures) on your submissions. You
shall consider this feedback when preparing the next assignment or
re-submissions.

The goal of assignments 1, 2 and 3 is to practice a controlled process for
creating and documenting a software architecture, with an understanding of how
to transform quality goals into a practical architecture solution, and to apply
appropriate architecture styles and patterns in this process. The design method
by Hofmeister et al. must be used when designing and documenting the
architecture. The architecture must be evaluated with respect to relevant
quality requirements and, if necessary, transformed until the quality
requirements are met. 

The goal of assignment 4 is to introduce a formal language for architecture
specification and evaluation. 

Assignment 5 (individual) consists of a set of questions, related to both
theory taught in the lectures and practical experience acquired in the
assignments.

\section{Assignment 1: Architecture Decisions}
\begin{quote}
System analysis and architectural decisions
\end{quote}

\noindent You will be provided with a system description on the course
homepage. This document contains a general description of the system and its
intended use. Based on this information, you are supposed to extract
requirements and define additional requirements if necessary.

You are then expected to construct factor tables, issue cards, and relevant
strategies for solving the issues. Bear in mind that the strategies must
primarily be \emph{software}-related, and wherever you use tactics from Bass et
al. or any particular architecture style, this shall be properly referenced.
All your strategies need to be well motivated in order to create a clear link 
between the issue itself and how the strategy addresses the issue.

Finally, you are expected to create a conceptual view of the system, where 
relevant strategies are implemented, \emph{with} traceability links back to the
relevant issue cards.

\subsection{Deliverables}
\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Introduction}] An introduction, presenting the assignment and the
system.
 \item [{Assumptions}] A description of assumptions you have on the nature of
the application domain, the system, the environment in which it will be
deployed, the customer and users.
 \item [{System analysis}] Factor tables with relevant factors (organizational
factors left out), issue cards and strategies.
 \item [{Conceptual View}] The analysis and design of the architecture according
to Hofmeisters' conceptual view. Motivate design choices by the developed
strategies!
\end{description}

\subsection{Actions}
\begin{enumerate}
 \item Submit the assignment on the course homepage
 \item Find a group with which you perform the peer evaluation. Exchange the
deliverables of assignment 1 AT LATEST 3 days before the peer evaluation
seminar.
\end{enumerate}

\subsection{Relevant study packages}
\begin{itemize}
 \item Architecture Fundamentals
 \item Architecture Documentation
\end{itemize}


\section{Assignment 2: Architecture Design}
\begin{quote}
Architecture design, evaluation and transformation
\end{quote}

\noindent Based on the results from the peer evaluation and the feedback 
received on assignment 1, continue to design the architecture. Choose an
evaluation method and evaluate the architecture against relevant quality
requirements. If necessary, apply transformations and re-evaluate. Iterate
evaluation and transformation until the quality requirements are met.

For Architecture Transformations, see Bosch 2000.

\subsection{Deliverables}

\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Assignment 1}] The content of assignment 1, adding and highlighting
changes that emerged from the peer evaluation and feedback.
 \item [{Architecture design}] Applying Hofmeisters' method, design the
module and the execution view. Motivate design choices by the developed
strategies!
 \item [{Evaluation method}] Provide information on the chosen
architecture evaluation method, along with a motivation why it was chosen (also
stating why other methods were rejected).
 \item [{Evaluation result}] Present the result from the architecture
evaluation.
 \item [{Transformation strategy}] If transformation is necessary, describe and
motivate the chosen transformation strategy.
 \item [{Transformation step}] Apply the transformation on the architecture,
highlighting the changes in the architecture design. It is important to
maintain a chain of evidence: issue discovered in the evaluation $\rightarrow$
chosen transformation $\rightarrow$ impact on the architecture and architecture decisions.
If strategies are added/modified, document the changes.
 \item [{Re-evaluate}] In order to see whether the transformation had the
desired effect on the architecture, you need to perform a second evaluation.
Document the results and re-iterate (evaluate, choose transformation strategy,
transform, re-evaluate) if the architecture still does not meet the quality
requirements.
\end{description}

\subsection{Actions}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}

\subsection{Relevant study packages}
\begin{itemize}
 \item Architecture Evaluation 
 \item Architecture Transformations
\end{itemize}



\section{Assignment 3: Architecture Change}

\begin{quote}
Changeability, anyone?
\end{quote}

\noindent In this assignment we introduce changes to the initial system 
description. You need to analyze whether your architecture
design still fulfills the requirements and trace any and all changes.

\subsection{Deliverables}
\begin{description}
 \item [{Title page}] Course title and number, name of the system, assignment
number, name and social id of group members. A table where you describe
the contribution to ideas and documentation of each team member (in percent).
 \item [{Assignment 2}] The content of assignment 2, adding and highlighting
changes that emerged from the feedback
 \item [{Change scenarios}] Motivate your choice of change scenarios and why
you don't address the remaining ones. 
 \item [{Impact analysis}] Analyze the impact of the system changes on factors,
issue cards and developed strategies. Answer the following questions:
 \begin{enumerate}
  \item Which factors have changed? How?
  \item If factors have changed, which areas in the architecture are affected?
  \item Is it necessary to introduce new factors? Which?
  \item Do the existing issue cards, and the corresponding strategies, consider
and cope with the changes? If not, how shall they be changed?
 \end{enumerate}
 \item[{Implemented changes}] The updates to your architecture.
 \item [{Evaluation method}] Motivate why you either re-apply the evaluation
method from assignment 2 or choose a different method to assess whether your
architecture fulfills the new/modified quality requirements.
 \item [{Evaluation result}] Present the result from the architecture
evaluation.
 \item [{Transformation strategy}] If transformation is necessary, describe and
motivate the chosen transformation strategy.
 \item [{Transformation step}] Apply the transformation on the architecture,
highlighting the changes in the architecture design. It is important to
maintain a chain of evidence: issue discovered in the evaluation $\rightarrow$
chosen transformation $\rightarrow$ impact on the architecture. If strategies
are added/modified, document the changes.
 \item [{Re-evaluate}] In order to see whether the transformation had the
desired effect on the architecture, you need to perform a second evaluation.
Document the results and re-iterate (evaluate, choose transformation strategy,
transform, re-evaluate) if the architecture does not meet the quality
requirements.
\end{description}

\subsection{Actions}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}

\subsection{Relevant study packages}
\begin{itemize}
 \item Architecture Evaluation 
 \item Architecture Transformations
\end{itemize}

\section{Assignment 4: Architecture Evaluations}
\begin{quote}
Formal architecture specification and evaluation
\end{quote}

\noindent In this assignment you will extend an existing architecture model 
represented in AADL and perform resource allocation and latency evaluations.

The system description, the architecture model and a more detailed
description of this assignment are provided in a separate document and are
available on the course homepage.

\subsection{Actions}
\begin{enumerate}
 \item Submit the assignment on the course homepage.
\end{enumerate}

\subsection{Relevant study packages}
\begin{itemize}
 \item AADL
 \item Architecture Evaluation
\end{itemize}

\section{Assignment 5: Post-mortem}
% Find exam questions, come up with some new ones...
\end{document}
